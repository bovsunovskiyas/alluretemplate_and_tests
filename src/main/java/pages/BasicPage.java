package pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class BasicPage {

    public void clickButton(String text) { $(By.xpath("//a[text()='" + text + "']")).click();
    }
    public void clickButtonSpan(String text) {$(By.xpath("//span[text()='" + text + "']/..")).click();
    }

    public void clickButtonLink(String text) { $(By.xpath("//button[text()='" + text + "']")).click();
    }

    public void clickButtonLinkA(String text) { $(By.xpath("//a[text()='" + text + "']")).click();
    }

    public void clickButtonDiv(String text) { $(By.xpath("//div[text()='" + text + "']")).click();
    }

    public void clickButtonHardLocator() { $(By.xpath("//*[@viewBox='0 0 24 24' and @width='24' and @height='24']")).click();
    }




    public void contentIsVisible(String text) { $(By.xpath("//*[text()='" + text + "']")).shouldBe(Condition.visible);
    }


    public void clickButtonTag(String text) { $(By.xpath("//button[text()='" + text + "']")).click();
    }


    public Boolean checkTextExist(String text) {
        if ($(By.xpath("//*[text()='" + text + "']")).exists()){
            return true;
        }
        else return false;
    }


    public void clickButtonLi(String text) { $(By.xpath("//li[text()='" + text + "']")).click();

    }
}
