package steps;

import config.UserConfig;
import impl.AuthServiceImpl;
import io.cucumber.java.en.Then;
import io.restassured.http.ContentType;
import io.restassured.internal.common.assertion.Assertion;
import io.restassured.response.Response;
import models.api.User;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import pages.SignInPage;
import services.AuthService;

import static app_context.RunContext.get;
import static app_context.RunContext.put;
import static io.restassured.RestAssured.given;


public class SignInPageDef {
    SignInPage signInPage = new SignInPage();
    AuthService authService = new AuthServiceImpl();


    @Then("Input login")
    public void inputLogin() {
        signInPage.inputLogin(UserConfig.USER_LOGIN);
    }

    @Then("Input password")
    public void inputPassword() {
        signInPage.inputPassword(UserConfig.USER_PASSWORD);
    }

    @Then("Create user by API")
    public void createUserByAPI() {
        User newUser = User.builder()
                .email("auto_" + RandomStringUtils.randomAlphabetic(5) + "@.mail.com")
                .firstName("TestFirstName")
                .lastName("TestLastName")
                .password(UserConfig.USER_PASSWORD)
                .rememberMe(true)
                .pwdless("true")
                .janusId("test")
                .headless(false)
                .testMode(false)
                .build();
        put("user", newUser);

        User createdUser = authService.createUser(newUser);
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(newUser.getEmail().toLowerCase(), createdUser.getEmail().toLowerCase());
        Assert.assertNotNull(createdUser.getId());
    }

    @Then("Login user by API")
    public void loginUserByAPI() {
        User user = get("user", User.class);
        User userLogged = authService.login(user);
        System.out.println(userLogged.toString());
        Assert.assertNotNull(userLogged);
    }

    @Then("Login user by API2")
    public void loginUserByAPI2() {

        System.out.println("Start login2 method");
        
        //User loggedUser = authService.login(user);
        System.out.println("Usage1 enter data of existing user");

        User existingUser = User.builder()
                .email("My_test_user@mail.com")
                .pwdless("true")
                .janusId("test")
                .headless(false)
                .testMode(false)
                //.password(UserConfig.USER_PASSWORD)
                .password("Password1@!")
                .rememberMe(true)
                .build();


        System.out.println("1) existingUser before put command:" + existingUser);
        System.out.println("1) existingUser id before put command: " + existingUser.getId());



        put("user", existingUser);

        System.out.println("2) existingUser after put command:" + existingUser);
        System.out.println("2) existingUser id after put command: " + existingUser.getId());


        User userLogged = authService.login(existingUser);

        System.out.println("3) existingUser after put command and  User userLogged = authService.login(existingUser):" + existingUser);
        System.out.println("3) existingUser id after put command and  User userLogged = authService.login(existingUser): " + existingUser.getId());


        System.out.println("Usage xxxx");
        System.out.println("4) User userLogged:" + userLogged);
        System.out.println("4) UserID userLogged: " + userLogged.getId());

        //User user = get("user", User.class);




        


        //put("user", existingUser);

        //System.out.println(loggedUser.toString());
        System.out.println("===========");
        System.out.println("===========: " + existingUser.getId());
        Assert.assertEquals(existingUser.getEmail().toLowerCase(), userLogged.getEmail().toLowerCase());
        Assert.assertNotNull(existingUser);
        Assert.assertNotNull(userLogged.getId());
        //User createdUser = authService.createUser(newUser);
//          Assert.assertNotNull(existingUser);
//        Assert.assertEquals(existingUser.getEmail().toLowerCase(), createdUser.getEmail().toLowerCase());
//        Assert.assertTrue(createdUser.getId() != null);
//        Assert.assertNotNull(createdUser.getId());
    }
}
